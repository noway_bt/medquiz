package com.example.medquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import static com.example.medquiz.MainActivity.CORRECT_ANSWERS;
import static com.example.medquiz.MainActivity.NUMBER_OF_QUESTIONS;
import static com.example.medquiz.MainActivity.WRONG_LIST_ANSWERS;

public class SummaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        ListView lv = findViewById(R.id.list);
        List<QandA> qandAList = (List<QandA>) getIntent().getSerializableExtra(WRONG_LIST_ANSWERS);
        lv.setAdapter(new AnswersAdapter(getApplicationContext(), qandAList));
        TextView totalScore = findViewById(R.id.total_answers);
        totalScore.setText(String.valueOf(getIntent().getIntExtra(NUMBER_OF_QUESTIONS,0)));

        TextView correctAnswers = findViewById(R.id.correct_answers);
        correctAnswers.setText(String.valueOf(getIntent().getIntExtra(CORRECT_ANSWERS,0)));
    }
}
