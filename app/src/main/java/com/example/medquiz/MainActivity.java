package com.example.medquiz;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.medquiz.StartActivity.ANSWERS_RESOURCE_ID;
import static com.example.medquiz.StartActivity.LITERATURE_RESOURCE_ID;
import static com.example.medquiz.StartActivity.QUESTIONS_RESOURCE_ID;

public class MainActivity extends AppCompatActivity {

    public static final String WRONG_LIST_ANSWERS = "wrongListAnswers";
    public static final String NUMBER_OF_QUESTIONS = "numberOfQuestions";
    public static final String CORRECT_ANSWERS = "correctAnswers";
    private final String CURRENT_QUESTION = "CurrentQuestion";
    private QuestionsList questionsList;
    private Integer questionNumber = 0;
    private Map<Integer, String> answerMap;
    private Map<Integer, String> literatureMap;
    private Integer correctAnswers = 0;
    private List<QandA> wrongAnswers;
    private List<Integer> numbersOfWrongAnswers;
    private TextView textView_answer_A;
    private TextView textView_answer_B;
    private TextView textView_answer_C;
    private TextView textView_answer_D;
    private Button nextQuestion;
    private Button buttonA;
    private Button buttonB;
    private Button buttonC;
    private Button buttonD;
    private ImageButton literature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_constraint);
       // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        int questionsResourceId = getIntent().getIntExtra(QUESTIONS_RESOURCE_ID, 0);
        int answersResourceId = getIntent().getIntExtra(ANSWERS_RESOURCE_ID, 0);
        int literatureResourceId = getIntent().getIntExtra(LITERATURE_RESOURCE_ID, 0);
        questionsList = new QuestionsList(readQuestions(questionsResourceId));
        answerMap = readDataToMap(answersResourceId);
        literatureMap = readDataToMap(literatureResourceId);
        wrongAnswers = new ArrayList<>();
        fillQuestionFields(questionNumber);

        textView_answer_A = findViewById(R.id.answer_A);
        textView_answer_B = findViewById(R.id.answer_B);
        textView_answer_C = findViewById(R.id.answer_C);
        textView_answer_D = findViewById(R.id.answer_D);
        nextQuestion = findViewById(R.id.next);
        buttonA = findViewById(R.id.button_A);
        buttonB = findViewById(R.id.button_B);
        buttonC = findViewById(R.id.button_C);
        buttonD = findViewById(R.id.button_D);
        literature = findViewById(R.id.bookImage);
        if(literatureMap.get(questionNumber+1) != null) {
            literature.setVisibility(View.VISIBLE);
        } else {
            literature.setVisibility(View.INVISIBLE);
        }

        nextQuestion.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        questionNumber = savedInstanceState.getInt(CURRENT_QUESTION);
        fillQuestionFields(questionNumber);
        correctAnswers = savedInstanceState.getInt(CORRECT_ANSWERS);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_QUESTION, questionNumber);
        outState.putInt(CORRECT_ANSWERS, correctAnswers);
        super.onSaveInstanceState(outState);
    }


    private void fillQuestionFields(int index) {
        TextView textView = findViewById(R.id.number);
        textView.setText(questionsList.getQuestion(index).getQuestNumber() + ".");

        TextView textView1 = findViewById(R.id.question);
        textView1.setText(questionsList.getQuestion(index).getQuestion());

        TextView textView2 = findViewById(R.id.answer_A);
        textView2.setText(questionsList.getQuestion(index).getAnswerA());

        TextView textView3 = findViewById(R.id.answer_B);
        textView3.setText(questionsList.getQuestion(index).getAnswerB());

        TextView textView4 = findViewById(R.id.answer_C);
        textView4.setText(questionsList.getQuestion(index).getAnswerC());

        TextView textView5 = findViewById(R.id.answer_D);
        textView5.setText(questionsList.getQuestion(index).getAnswerD());
    }

    private List<Question> readQuestions(int questionsResourceId) {
        InputStream is = getResources().openRawResource(questionsResourceId);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8")));
        String line = "";
        List<Question> questions = new ArrayList<>();
        try {
            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    questions.add(QuestionFactory.create(line));
                }
            }
        } catch (Exception e) {
            Log.e("MainActivity", "Error" + line, e);
            e.printStackTrace();
        }
        return questions;
    }

    private Map<Integer, String> readDataToMap(int resourceId) {
        if(resourceId == 0) {
            return new HashMap<>();
        }
        InputStream is = getResources().openRawResource(resourceId);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8")));
        String line = "";
        Map<Integer, String> resultMap = new HashMap<>();
        try {
            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] singleData = line.split(";");
                    resultMap.put(Integer.valueOf(singleData[0]), singleData[1].trim());
                }
            }
        } catch (IOException e) {
            Log.e("MainActivity", "Error" + line, e);
            e.printStackTrace();
        }
        return resultMap;
    }


    public void onClickAnswer(View view) {
        String answer = "";
        switch (view.getId()) {
            case R.id.answer_A:
            case R.id.button_A:
                answer = "A";
                break;
            case R.id.answer_B:
            case R.id.button_B:
                answer = "B";
                break;
            case R.id.answer_C:
            case R.id.button_C:
                answer = "C";
                break;
            case R.id.answer_D:
            case R.id.button_D:
                answer = "D";
                break;
        }
        
        String correctAnswer = answerMap.get(questionNumber + 1);
        if (answer.equals(correctAnswer)) {
            correctAnswers++;
        } else {
            Question question = questionsList.getQuestion(questionNumber);
            wrongAnswers.add(new QandA(question.getQuestNumber(), question.getQuestion(), question.getAnswerContent(correctAnswer)));
            setButtonColor(answer, R.drawable.bad_answer_rnd);

        }
        setButtonColor(correctAnswer, R.drawable.good_answer_rnd);

        setAnswerEnable(false);
    }

    public void onClickLiterature(View view) {
        String literatureContent = literatureMap.get(questionNumber+1);
        if(literatureContent != null && !literatureContent.isEmpty()) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Literatura");
            alertDialog.setMessage(literatureContent);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    private void setButtonColor(String correctAnswer, int bkgRes) {
        switch (correctAnswer) {
            case "A":
                buttonA.setBackgroundResource(bkgRes);
                break;
            case "B":
                buttonB.setBackgroundResource(bkgRes);
                break;
            case "C":
                buttonC.setBackgroundResource(bkgRes);
                break;
            case "D":
                buttonD.setBackgroundResource(bkgRes);
                break;
        }

    }

   /* private void setAnswerColor(String correctAnswer, int color) {
        switch (correctAnswer) {
            case "A":
                textView_answer_A.setBackgroundColor(color);
                break;
            case "B":
                textView_answer_B.setBackgroundColor(color);
                break;
            case "C":
                textView_answer_C.setBackgroundColor(color);
                break;
            case "D":
                textView_answer_D.setBackgroundColor(color);
                break;
        }
    }*/

    private void restoreColors() {
        buttonA.setBackgroundResource(R.drawable.rounded_button);
        buttonB.setBackgroundResource(R.drawable.rounded_button);
        buttonC.setBackgroundResource(R.drawable.rounded_button);
        buttonD.setBackgroundResource(R.drawable.rounded_button);

       // setButtonColor("A", Color.LTGRAY);

    }

    private void setAnswerEnable(boolean enable) {
        textView_answer_A.setClickable(enable);
        textView_answer_B.setClickable(enable);
        textView_answer_C.setClickable(enable);
        textView_answer_D.setClickable(enable);
        buttonA.setClickable(enable);
        buttonB.setClickable(enable);
        buttonC.setClickable(enable);
        buttonD.setClickable(enable);
        if (enable) {
            nextQuestion.setVisibility(View.INVISIBLE);
        } else {
            nextQuestion.setVisibility(View.VISIBLE);
        }
    }

    public void nextQuestion(View view) {
        questionNumber++;
        if (questionNumber >= questionsList.size()) {
            Intent intent = new Intent(MainActivity.this, SummaryActivity.class);
            intent.putExtra(NUMBER_OF_QUESTIONS, questionsList.size());
            intent.putExtra(CORRECT_ANSWERS, correctAnswers);
            intent.putExtra(WRONG_LIST_ANSWERS, (Serializable) wrongAnswers);
            startActivity(intent);
            finish();
        } else {
            fillQuestionFields(questionNumber);
            setAnswerEnable(true);
        }
        restoreColors();
    }
    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.navigation_menu, menu);
//        return true;
//    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LinearLayout main = findViewById(R.id.background);
        switch (item.getItemId()) {

            case R.id.amber:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.amber_gradient));
                break;

            case R.id.aqua:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.aqua_gradient));
                break;

            case R.id.blue:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_gradient));
                break;

            case R.id.green:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.green_gradient));
                break;

            case R.id.grey:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.grey_gradient));
                break;

            case R.id.pink:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.pink_gradient));
                break;

            case R.id.purple:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.purple_gradient));
                break;
            case R.id.mix:
                main.setBackgroundDrawable(getResources().getDrawable(R.drawable.mix_gradient));
                break;
            default:
                break;
        }

        return true;
    }*/
}
