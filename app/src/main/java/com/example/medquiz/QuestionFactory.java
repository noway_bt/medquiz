package com.example.medquiz;

public class QuestionFactory {

    public static final String FIELD_SEPARATOR = ";";
    public static final int QUESTION_NUMBER_COLUMN = 0;
    public static final int QUESTION_COLUMN = 1;
    public static final int ANSWER_A_COLUMN = 2;
    public static final int ANSWER_B_COLUMN = 3;
    public static final int ANSWER_C_COLUMN = 4;
    public static final int ANSWER_D_COLUMN = 5;

    public static Question create(String line) {
        String[] fields = line.split(FIELD_SEPARATOR);
        return new Question(
                Integer.valueOf(fields[QUESTION_NUMBER_COLUMN]),
                fields[QUESTION_COLUMN],
                fields[ANSWER_A_COLUMN],
                fields[ANSWER_B_COLUMN],
                fields[ANSWER_C_COLUMN],
                fields[ANSWER_D_COLUMN]);
    }
}
