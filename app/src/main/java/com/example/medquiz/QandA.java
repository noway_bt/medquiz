package com.example.medquiz;

import java.io.Serializable;

public class QandA implements Serializable {
    private int questionNumber;
    private String contentQuestion;
    private String correctAnswer;

    public QandA(int questionNumber, String contentQuestion, String correctAnswer) {
        this.questionNumber = questionNumber;
        this.contentQuestion = contentQuestion;
        this.correctAnswer = correctAnswer;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public String getContentQuestion() {
        return contentQuestion;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }
}
