package com.example.medquiz;

public class Question {
    private int questNumber;
    private String question;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;

    public Question(int questNumber, String question, String answerA, String answerB, String answerC, String answerD) {
        this.questNumber = questNumber;
        this.question = question;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
    }

    public int getQuestNumber() {
        return questNumber;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswerA() {
        return answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public String getAnswerContent(String answerLetter) {
        switch (answerLetter) {
            case "A":
                return answerA;
            case "B":
                return answerB;
            case "C":
                return answerC;
            case "D":
                return answerD;
            default:
                throw new RuntimeException("no matched answer");
        }
    }

}
