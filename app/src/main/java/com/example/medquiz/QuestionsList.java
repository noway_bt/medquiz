package com.example.medquiz;

import java.util.List;

public class QuestionsList {
    private List<Question> questionList;

    public QuestionsList(List<Question> questionList) {
        this.questionList = questionList;
    }

    public Question getQuestion(int index) {
        return questionList.get(index);
    }

    public int size() {
        return questionList.size();
    }
}
