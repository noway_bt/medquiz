package com.example.medquiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AnswersAdapter  extends ArrayAdapter<QandA> {

    public AnswersAdapter(Context context, List<QandA> qAndAList) {
        super(context, 0, qAndAList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if there is an existing list item view (called convertView) that we can reuse,
        // otherwise, if convertView is null, then inflate a new list item layout.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.correct_answer_item, parent, false);
        }

        // Find the article at the given position in the list of articles
        QandA currentArticle = getItem(position);

        // Find the TextView with view ID section
        TextView sectionView = (TextView) listItemView.findViewById(R.id.questionNumber);
        // Display the section name of the current article in that TextView.
        Integer questionNumber = currentArticle.getQuestionNumber();
        sectionView.setText(questionNumber.toString());



        TextView questionContent = (TextView) listItemView.findViewById(R.id.questionContent);
        questionContent.setText(currentArticle.getContentQuestion());

        // Find the TextView with view ID title.
        TextView correctAnswer = (TextView) listItemView.findViewById(R.id.correctAnswer);
        // Display the title of the article.
        String title = currentArticle.getCorrectAnswer();
        correctAnswer.setText(title);
        // Return the list item view that is now showing the appropriate data
        return listItemView;
    }
}
