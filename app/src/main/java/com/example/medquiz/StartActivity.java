package com.example.medquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.io.Serializable;

public class StartActivity extends AppCompatActivity {
    public static final String QUESTIONS_RESOURCE_ID = "QUESTION_RESOURCE_ID";
    public static final String ANSWERS_RESOURCE_ID = "ANSWER_RESOURCE_ID";
    public static final String LITERATURE_RESOURCE_ID = "LITERATURE_RESOURCE_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void startSpring2018test(View view) {
        chooseTest(R.raw.wiosna_2018_pytania, R.raw.wiosna_2018_odpowiedzi, R.raw.wiosna_2018_literatura_test);
    }

    public void startAutumn2018test(View view) {
        chooseTest(R.raw.jesien_2018_pytania, R.raw.jesien_2018_odpowiedzi, R.raw.jesien_2018_literatura);
    }

    public void startSpring2019test(View view) {
        chooseTest(R.raw.wiosna_2019_pytania, R.raw.wiosna_2019_odpowiedzi, 0);
    }

    public void startAutumn2019test(View view) {
        chooseTest(R.raw.jesien_2019_pytania, R.raw.jesien_2019_odpowiedzi, 0);
    }

    private void chooseTest(int questionResource, int answerResource, int literatureResource) {
        Intent intent = new Intent(StartActivity.this, MainActivity.class);
        intent.putExtra(QUESTIONS_RESOURCE_ID, questionResource);
        intent.putExtra(ANSWERS_RESOURCE_ID, answerResource);
        intent.putExtra(LITERATURE_RESOURCE_ID, literatureResource);
        startActivity(intent);
    }
}
